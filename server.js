const io = require('socket.io')();

var durations = [6, 12, 24, 36];
var monthInYear = 12;
var coef = 0.20;


io.on('connection', (client) => {
    client.on('subscribeToTimer', (interval) => {
        console.log('client is subscribing to timer with interval ', interval);
        setInterval(() => {
            client.emit('timer', new Date());
        }, interval);
    });

    client.on('getInitial', () => {
        client.emit('setInitial', { 'duration': durations, 'coef': coef });
    });

    client.on('calculate', (data) => {
        console.log(data);
        let result = data.value * coef * data.duration / monthInYear + Number(data.value);
        let perMonth = result / data.duration;
        perMonth = Math.round(perMonth * 100) / 100;
        result = Math.round(result * 100) / 100;
        client.emit('result', { 'result': result, 'monthlyPayment': perMonth });
    });
});


const port = 8000;
io.listen(port);
console.log('listening on port ', port);



import React, { Component } from 'react';
import openSocket from 'socket.io-client';
//Разные ставки, время 
const socket = openSocket('http://localhost:8000');

class App extends Component {
    constructor(props){
        super(props);
        this.state = {
            duration: [],
            activeDuration: 'Choose duration',
            amount: 0,
            result: 0,
            monthlyPayment: 0,
            coef: 0
        };
    }

    onChangeAmount = (event) => {
        this.setState({ amount: event.target.value });
    }

    onChangeDuration = (event) => {
        if (event.target.duration !== this.state.activeDuration) {
            this.setState({ activeDuration: event.target.value });
        }
    }

    onSubmit = (event) => {
        event.preventDefault();
        socket.emit('calculate', { 'value': this.state.amount, 'duration': this.state.activeDuration });
    }

    componentDidMount() {
        socket.on('setInitial', (respond) => {
            this.setState({ coef: respond.coef, duration: respond.duration });
        });

        socket.on('result', (respond) => {
            console.log(respond);
            this.setState({ result: respond.result, monthlyPayment: respond.monthlyPayment });
        });

        socket.emit('getInitial');
    }


    render() {
        let options = [];
        let select;

        options.push(<option key="" disabled="disabled" hidden="hidden" selected>
            {this.state.activeDuration}
        </option>);

        if (this.state.duration) {
            for (var i = 0; i < this.state.duration.length; i++) {
                options.push(
                    <option key={this.state.duration[i]} >
                        {this.state.duration[i]}
                    </option>
                );
            }
        }
        
        select = <select className="calc-form_select" selected={this.state.activeDuration} onChange={this.onChangeDuration}>{options}</select>;

        return (
            <div>
                <form className="calc-form" onSubmit={this.onSubmit}>
                    <input className="calc-form_input" value={this.state.amount} onChange={this.onChangeAmount} />
                    <p className="calc-form_message"> Coeff: {this.state.coef}</p>
                    {select}

                    <button className="calc-form_submit">Submit</button>
                </form>
                <div className="calc-result">
                    <p className="calc-result_message">You will pay:</p>
                    <input className="calc-result_input-monthly" disabled value={"Per month: " + this.state.monthlyPayment} />
                    <input className="calc-result_input-total" disabled value={"Totally: " + this.state.result} />
                </div>   
            </div>
        );
  }
}

export default App;
